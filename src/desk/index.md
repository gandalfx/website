---
title: My Desk
meta:
  - name: 'twitter:title'
    content: 'My Desk'
  - name: 'twitter:description'
    content: 'My desk setup as a remote developer evangelist.'
  - name: 'og:title'
    content: 'My Desk'
  - name: 'og:description'
    content: 'My desk setup as a remote developer evangelist.'
---

# My Desk

I sometimes get asked about my desk setup, and I figured it would be nice to share that easily with folks.

Ever since the pandemic, many more folks are working from home and trying to understand how to make that work. I've shared several tips for being [Suddenly Remote](/talks/2020-05-05-suddenly-remote.html) before, but I thought a deep dive into my setup would help some.

<!-- For more details on the setup see [my most recent blog post on how I do remote tech]().  -->

This page will serve as the evergreen spot for my **current** setup and tech.

## Current Setup

<Desk/>

1. [Elgato Wave:1 Microphone](https://www.elgato.com/en/wave-1) - this is my "daily driver" microphone. And the Wave Link software is what I use when streaming on [Twitch](https://twitch.tv/olearycrew)
2. [Elgato Stream Deck](https://amzn.to/34PFglS) - comes in various sizes. Controls lights, camera, microphone
3. [IQUNIX F96](https://iqunix.store/collections/all) - great bluetooth 96% mechanical keyboard with Cherry MX Brown switches
4. [DinoFire Presentation Clicker](https://amzn.to/3dusRYO) - I use this even when presenting from my desk because it is quieter than a keyboard or mouse click.
5. [Apple AirPods Pro](https://amzn.to/3lNV1R8) - daily driver for listening to video calls, music while working
6. [Logitech MX Master 2S](https://amzn.to/3nRWAzF)
7. [In ear monitors](https://amzn.to/3dxBbH5) and [extension cable](https://amzn.to/33UmLO1) - for presentations and recordings where I want a professional look. Used in conjunction with audio interface below
8. [MOTU M2 USB-C Audio Interface](https://amzn.to/3nTd6zs) - this drives the SM7B below, gives it 48v of phantom power and connects it to my Macbook
9. [Electric Stand up Desk Frame](https://amzn.to/2H73y2x) - the desk can be a standing or sitting desk. The top I made myself from some maple hardwood...but this is the kit I used to motorize it.
10. [RODE PS 1 Microphone Boom Arm](https://amzn.to/2GYp1e9)
11. [Shure SM7B](https://amzn.to/3lJ5Ow6) - go to microphone for podcasters, this is the one I use to sound amazing on Zoom or presentations and to...well record [my podcast](https://doyouevenart.com/). Yes it is super expensive but I'll tell you why I think it's worth it if you ask.
12. [Glide Gear TMP100](https://amzn.to/3lMuLqA) iPad telepromter I use for recording formal videos. For daily driver teleprompting I use [telepromt.me](https://teleprompt.me/)
13. [Elgato Key Light](https://amzn.to/2SVB5Qe) I have two of these. They are expensive but they are amazing. Good lighting is required for good video
14. [Sony Alpha A1000](https://amzn.to/33XrK0n) - Mirrorless camera paired with a [dummy battery](https://amzn.to/33WEJiO) and an [Elgato CamLink](https://amzn.to/34Yemsk) to get professional quality video into my Macbook. I bought mine used for a great price.
15. ☕

### MacBook

My MacBook isn't labeled above but is a 15" 2018 MacBook Pro.

- **Processor**: 2.9 GHz 6-core Intel Core i9
- **Memory**: 32 GB 2400 MHz DDR4
- **Graphics**: Radeon Pro 560X 4 GB
- **Hard Drive**: 500 GB

#### Apps I use

Below is a list of apps that I use on my Mac. I've put a ❤️ next to ones I love.

- ❤️ [Bartender 4](https://www.macbartender.com/Bartender4/)
- ❤️ [f.lux](https://justgetflux.com/)
- ❤️ [Caffeine](https://www.intelliscapesolutions.com/apps/caffeine)
- [iStats Menus](https://bjango.com/mac/istatmenus/)

### Other Items

Other items that are not pictured:

- A much more advanced [Doosl Presentation Remote with 1000ft range](https://amzn.to/350MFyU) I use in person
- My [USB-C hub](https://amzn.to/3nON4Nr) and [second hub](https://amzn.to/2GPQcZ7) that makes it one plug to place and remove my laptop for power, HDMI and all the USB devices
- The normal cluter of mail, random fidgets and other stuff that is on the desk

> **Note, in the interest of transparency:** I am not currently "sponsored" by any of these products...but for the links that are to Amazon they _are_ affiliate links where I would get a small commission if you buy them there.
