---
title: Home
home: true
heroImage: /img/speaker02.png
heroText: Hi, I'm Brendan
heroEmoji: ['😃', '😺', '😁', '🙂', '🦊', '🐢', '☘️', '🍀', '🥃', '👨‍💻']
tagline: I'm a technologist, a DevOps fanatic, and speaker.
subtext: On this site you'll find my blog, talks, and more about me.
actionText: Get in touch
actionLink: /contact/
features:
  - title: Speaker
    href: /talks/
    link_text: See my talks
    details: Not your typical tech speaker.  From Apollo to Legos to Black Mirror, you won't find a "boring deck with code" here.
  - title: Technologist
    href: /portfolio/
    link_text: Check out my portfolio
    details: From bricking my first Tandy computer (sorry Dad) to building side apps people love, I have experience tinkering and building from scratch.
  - title: DevOps Expert
    href: '/work-philosophy/'
    link_text: Read my work philosophy
    details: My philosophy on software delivery comes from hard-won experience in a variety of industries.  I've seen what works and what DOESN'T.
#footer: This is a footer [foo](bar.com)
---

# Bio

<Bio/>

# Work

My entire career, my passion has been driven by the ability to level up engineers.

Originally this was as an Engineering and Product Manager at several software companies,
but more recently, I've been privileged to work on developer tooling directly at GitLab.
In 2020, I changed roles to be a [Developer Evangelist](https://about.gitlab.com/job-families/marketing/developer-evangelist/) at GitLab.

Learn more about my experience by viewing [my resume](/resume/) or [get in touch](/contact/).

## Talks

Please take a look at some of the [talks I've given](/talks/). There may also be some that are still a work in progress 😉.

## Portfolio

My [portfolio](/portfolio/) contains projects both past and present - and all have helped shaped my experience.

## Philosophy

See [my work philosophy](/work-philosophy/).

# Books I ❤️

See [books](/books/) for a list of books I love.

# Portfolio

See [my portfolio](/portfolio/) for more details on past and present work.
