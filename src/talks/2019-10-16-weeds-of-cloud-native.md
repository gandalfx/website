---
title: The Weeds of Cloud Native
talk: true
tagline: Navigating the weeds of Cloud Native tooling can be costly - this talk presents a framework for tooling decision making for the enterprise.
abstract: |
    In the era of cloud computing, Kubernetes has emerged as an engine for delivering reliable IT operations. Today, for organizations adopting DevOps, cloud native technology such as Kubernetes, microservices and serverless can dramatically accelerate development velocity and improve release frequency when paired with the right tooling. In this session, we will discuss - What is cloud native? Why is it important? What are the challenges of going cloud native in the enterprise? The fragmentation and hidden cost of the tooling available Solutions for enterprises dealing with these challenges. 
slides: https://docs.google.com/presentation/d/e/2PACX-1vRlkGH1skvQi9HjQmD61JvZUwzvV3pugVswL8vGzt8MLC1PDVrPG5mTJxxrwg1512BoVdgiZGZUrukt/embed
#video: https://www.youtube.com/
#events:
#  - name: A Great Event
#    date: "2020-02-02"
#    href: https://www.youtube.com/
---

<Talk :talk="$page.frontmatter"/>
