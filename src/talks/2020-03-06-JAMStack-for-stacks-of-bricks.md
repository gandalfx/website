---
title: "Expanding JAMstack with Professional-grade CI/CD"
talk: true
tagline: The JAMStack provides developers the ability to ship applications quickly and delight the people that matter most - users.  This talk tells that story.
abstract: |
    The JAMStack provides developers the ability to ship applications quickly and provide value for their users rather than spending time on their infrastructure. However,  understanding the best practices around CI and CD learned over the past decade is critical for large-scale production applications.  That is - the ability to reliably and repeatable build, test, deploy review applications and deploy to production - is critical for collaborating and delivering better software faster. Marrying the JAMStack with professional-grade CI/CD enables us to expand the reach of the stack and increase its overall velocity.
    
    In this talk, we'll use a fun example - my daughter's love of everything Lego and children's unending ability to ask questions - to illustrate this point. I want to build an application to delight my most crucial stakeholder - my daughter - but I also want to set myself up for success in the long term. How can I apply a modern CI/CD application like GitLab to ensure that everyone can contribute to my project, and it won't be just another JAMStack project that gets written and then put on the shelf?
#slides: https://docs.google.com/presentation/
#video: https://www.youtube.com/watch?v=Qwins6ZScCo
#events:
#  - name: ESCAPE/19 the multi-cloud conf
#    date: "2019-10-16"
#    href: https://www.youtube.com/watchv=cRGjw04ZA4M
---

<Talk :talk="$page.frontmatter"/>
