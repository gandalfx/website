---
title: Talks
layout: Talks
---

## My Talks

For a list of all of my abstracts, see [my abstracts](https://www.notion.so/boleary/b3191712dba24a859ab859f12e289e25?v=0563c1c5fdd04c5594bb76c2a029381a)

See my [events](https://www.notion.so/boleary/85eb5248a51c4389b9da4207b7b9d3d3?v=82a5fe489b384badadafe8cbe6a1fece) page for upcoming events. I even have what events I've submitted talks to buy may or may not be accepted to in the spirt of [transparency](https://about.gitlab.com/handbook/values/#transparency). You can also check out my [speaker bio](/talks/bio/).

## Previous Talks

[List of all of my speaking engagements](https://www.notion.so/boleary/85eb5248a51c4389b9da4207b7b9d3d3?v=c31eea3b3b664ef09f93b18297020ac1). You can also see a collection of many of my previous talks [on this YouTube playlist](https://www.youtube.com/playlist?list=PLHPUDUbFkW1bJOcOrMxpW6hnIHLOvThf2).
