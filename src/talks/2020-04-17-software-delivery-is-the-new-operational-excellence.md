---
title: Software delivery as the new operational excellence 
talk: true
tagline: Learn the four key software delivery metrics that are directly correlated to your organization's success
abstract: |
    Almost ten years ago, we heard that "software is eating the world."  In today's world, that phrase rings true in every industry - software has eaten the world.  In this new era, the critical differentiating factor to success, regardless of your organization's mission, is the ability to deliver better software faster.
    
    In this talk, we'll examine why many organizations still struggle to achieve reliable, timely software delivery.  At the same time, we'll offer ways to increase product velocity - and show how the data prove that it is possible to increase velocity while also decreasing software faults and increasing security.

    How?  A focus on the DevOps platform and end-to-end cycle time lead to 4 key metrics of software delivery and operational excellence.  And those metrics are directly correlated to organizational success.

# Originally based on https://drive.google.com/file/d/1aB5NrAck8GdyyO3Q8LzllUTJwSZFaFB8/view?ths=true 
#slides: https://docs.google.com/presentation/
#video: https://www.youtube.com/
#events:
#  - name: A Great Event
#    date: "2020-02-02"
#    href: https://www.youtube.com/
---

<Talk :talk="$page.frontmatter"/>