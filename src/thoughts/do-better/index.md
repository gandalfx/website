---
title: 'Do Better: Building for inclusion and accessibility'
meta:
  - name: 'twitter:title'
    content: 'Do Better: Building for inclusion and accessibility'
  - name: 'twitter:description'
    content: 'How can we build inclusion and accessiblity INTO our designs?'
  - name: 'og:title'
    content: 'Do Better: Building for inclusion and accessibility'
  - name: 'og:description'
    content: 'How can we build inclusion and accessiblity INTO our designs?'
---

# DO BETTER

There are many challenging problems to solve in computer science, programming, and shipping code to production. Accessibility and inclusive design are NOT one of them - they are **solved** problems.

We need to be conscious of building inclusive from the beginning, work against our unconscious biases, and build with (not just 'for') everyone. That will create a world where _everyone can contribute_.

This page is a non-exhaustive list of resources that I've found valuable on this subject, both as I thought about how my own [name](/thoughts/name) can show the importance of validating everyone's experiences and as I wrote my talk [The imperative to stop being lazy and do better](https://www.notion.so/boleary/The-imperative-to-stop-being-lazy-and-do-better-a71fcea6b21e45cfadbbdf47a7fb60d2). If you know of other great resources I should add here, please either [send me a DM on Twitter](https://twitter.com/olearycrew) or [fork this site and make a merge request](https://gitlab.com/brendan/website).

> Come from the talk? You can find the [slides here](https://docs.google.com/presentation/d/1xi4iKq-VWi8gaZ_XGj2C2KlXfQCJFQFId2C9SuJa47s/edit?usp=sharing) and links from the talk [here](#from-the-talk)

<iframe width="560" height="315" src="https://www.youtube.com/embed/XB4cjbYywqg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Resources

### Videos

- [Designing for a more equitable world](https://www.youtube.com/watch?v=z9XKBgdOrHU), TED talk by Antionette Carroll
- [Creating an Accessible Digital Future](https://www.youtube.com/watch?v=Wb2X9kYEvXc), TEDxMIT by Judy Brewer
- [How I'm fighting bias in algorithms](https://www.youtube.com/watch?v=UG_X_7g63rY), TED talk by Joy Buolamwini

### Articles

- [To Build More-Inclusive Technology, Change Your Design Process ](https://hbr.org/2020/10/to-build-more-inclusive-technology-change-your-design-process), Harvard Business Review
- [10 ways to start your inclusive, accessible design journey](https://uxdesign.cc/10-ways-to-start-your-inclusive-accessible-design-journey-590c59dead8a) by Sarvnaz Taherian, Ph.D
- [Facial Recognition Is Accurate, if You’re a White Guy](https://www.nytimes.com/2018/02/09/technology/facial-recognition-race-artificial-intelligence.html), New York Times
- [The Importance of Accessibility in Technology](https://www.vultr.com/news/The-Importance-of-Accessibility-in-Technology/)

### Courses

> Unless otherwise noted, these courses are free

- [Introduction to Web Accessibility](https://www.edx.org/course/web-accessibility-introduction) from W3C on edX
- [Web Fundamentals: Accessibility](https://developers.google.com/web/fundamentals/accessibility) from Google
- [Advanced Web Accessibility](https://www.udacity.com/course/web-accessibility--ud891) by Google on Udacity
- [List of a11y courses on GitHub](https://github.com/accessibility/a11y-courses)

### Other

- [Inclusive Naming Initiative](https://inclusivenaming.org/) from the Linux Foundation
- [Digital a11y](https://www.digitala11y.com/)
- [Inclusive Components project](https://inclusive-components.design/)
- [Section 508](https://www.section508.gov/create/software-websites) (US government standards for accessibility)
- [Falsehoods Programmers Believe About Names](https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/)

### From the Talk

- [My Twitter list of name invalidations](https://twitter.com/olearycrew/timelines/1280856165358985217)
- [Twitter thread on Zoom and face decition](https://twitter.com/colinmadland/status/1307111816250748933)
- [Twitter thread on rejected names](https://twitter.com/candicepoon/status/1357850211477979136)
- [Wrongfully Accused by an Algorithm](https://www.nytimes.com/2020/06/24/technology/facial-recognition-arrest.html), New York Times
- [10 ways to start your inclusive, accessible design journey](https://uxdesign.cc/10-ways-to-start-your-inclusive-accessible-design-journey-590c59dead8a) by Sarvnaz Taherian, Ph.D
