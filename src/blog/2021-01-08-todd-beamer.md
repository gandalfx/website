---
title: "Todd Beamer"
date: 2021-01-09 02:00:00
#image: /img/blog/element5-digital-T9CXBZLUvic-unsplash.jpg
type: post
blog: true
author: brendan
excerpt: > 
 What happens when you have to choose America or yourself?
meta:
  - name: 'twitter:title'
    content: 'Todd Beamer'
  - name: 'twitter:description'
    content: 'What happens when you have to choose America or yourself?'
  - name: 'og:title'
    content: 'Todd Beamer'
  - name: 'og:description'
    content: 'What happens when you have to choose America or yourself?'
---

<BlogImage image="/img/blog/todd.jpg" caption="Todd Beamer" />

This is Todd Beamer. He and 2,976 other innocent people died on September 11th, 2001. I didn’t know any of them, but when I went and saw their names etched in metal at the site of the Twin Towers, I sought out Todd’s name. When I touched it, that was the time, the entire visit to ground zero, that I cried.

It was because of what he represented to me. He was on Flight 93 - the last of the four highjacked flights that day. The passengers had learned from talking to loved ones that this wasn’t a typical hostage situation. Planes had flown into the towers and the Pentagon. Their plane was headed at the Capitol Building. Todd was on the phone with a United Airlines supervisor and told her the passengers would storm the cockpit. The last words she heard him say to the other passengers were, “Let’s roll.”

Shortly after that, an altercation in the cockpit and Fight 93 crashed into a field in Pennsylvania. Forty people onboard gave their lives to protect Congress and the Capitol Building. 

19 years, 3 months, and 26 days later, the President, who had been lying to his supporters for two months about an election, incited them to take back the Capitol building by force. He spoke at a rally that later turned into a riot that threatened the life of the Vice President and Congress people.  It trashed and desecrated the building. It left 5 people dead. An insurrection against the US government aided and abetted by the President. 

We have a President who chooses himself over America at every turn—juxtaposed with 40 people who were just on a regular flight and decided in a few moments to choose America over their own lives. 

It’s always been America or Trump. If you don’t see that after this week, I pray you will stop, think, and listen to this: the choice is America or Trump.
