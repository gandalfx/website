---
title:  "How to learn anything"
date:   2020-08-07 05:00:00
author: brendan
type: post
blog: true
excerpt: > 
  I'm often asked how I learned Excel or Photoshop or Javascript.  The answer might surprise you...and it's how you can learn _anything_
meta:
  - name: "twitter:title"
    content: "How to learn anything"
  - name: "twitter:description"
    content: "I'm often asked how I learned Excel or Photoshop or Javascript.  The answer might surprise you...and it's how you can learn _anything_"
  - name: "og:title"
    content: "How to learn anything"
  - name: "og:description"
    content: "I'm often asked how I learned Excel or Photoshop or Javascript.  The answer might surprise you...and it's how you can learn _anything_"
---

## How did you learn X? (technology or otherwise)

A lot of times, I get asked: "How did you learn X." X could be anything, typically technology related. How did you learn HTML? How did you learn Javascript? How did you learn Excel? How did you learn Powerpoint?

There are two main ways that I've learned every tool I've ever come to be half-way decent at using:

1.  Don't be afraid to break something
1.  Have something that _you_ want to do with the tool

### Don't be afraid

The first is especially true with computers. When it comes to things like power tools, you should be terrified of breaking something or cutting off a critical part, etc. But when it comes to computers and programming and the like - try to decrease your fear of failure or "breaking" it as low as possible.

Computers are super resilient and don't need a lot of coddling - I'm not very nice to them. While they have interfaces that are increasingly supposed to seem more human, they are still very "dumb" machines (I'm looking at you, Alexa). They are at their core very binary (good computer joke, right?). It will work the way you want to, or it won't. Either way, there's always a way to save your work, copy a new file, so you don't break the first one, mash the "ctrl-z" or "undo" buttons and a myriad of other ways (have you tried rebooting) to fix any issue that comes up. And by always, I mean: "So close to always that you should just act like it's always because it's most of the time, but when it's not it does suck, but you'll be okay anyway." Much like a bug in your house or a snake in your yard: the computer is (should be) more afraid of you than you are of it.

### Have a goal

The second point - having a clear goal in mind - is almost more important than the first. It's (almost) impossible to learn Photoshop or Javascript from a book. That's not to say books aren't valuable. Books can teach you a lot about theory and solidify your understanding of a topic. But without the proper _motivation_ and desire to actually achieve a goal, knowing how to use a tool can only go so far. With most things, especially those around technology, it's learning by doing that will work the best.

I used to think it was just 'me' that learned this way - that it just was an aspect of my personality. And it probably does have something to do with that because I have lots of friends who learn a lot through careful study of a book or the documentation. However, I wouldn't discount the value of learning through doing just because of that. I think there is a part of the knowledge of something practical (how to use a tool) that comes from the practical application of that tool. And that kind of practical experience - that muscle memory if you well - can only come from actually doing.

So - don't be afraid. Don't worry! Try things and try and break them. But most importantly, have a goal in mind when you set out to learn something new. And make sure that goal is important to you - more important than even the goal of learning the tool. That goal will keep you motivated when the going gets tough.
