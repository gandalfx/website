---
title:  "Building a htTP Calculator in Svelte"
date:   2020-04-14 05:00:00
tags: 
  - useless-is-not-worthles
  - learning
#image: /img/blog/pihole1.png
type: post
blog: true
author: brendan
excerpt: > 
  This is a project that, backed by a Google Spreadsheet backend, will calculate how much time until *your* backend is going to need extra help during this crisis.  Based on historical TPP (toilet paper performance), predict your family's TTBOPT (time-to-bidet-or-paper-towels).
---


Ever since I heard [Rich Harris](https://twitter.com/Rich_Harris) on [The Changelog](https://changelog.com/podcast/332) talking about developing Svelte for use in journalism and at the NY Times, I've wanted to learn [Svelte JS](https://svelte.dev/). 

## What is Svelte?
Many folks are quick to assume that Svelte is "just another javascript framework."  While that's not entirely wrong, there are a number of things that make Svelte very special in that space.

First, and most importantly, Svelte compiles your code into a very small, vanilla JS package without any framework built-in, eliminating the need for your users to load a large JS file to activate the shim or "virtual DOM" those frameworks require.  In addition, Svelte doesn't rely on large boilerplate components or a separate state management layer. Both of these allow for much simpler code both at develop-time and runtime.  

While not a "big" deal on fast, desktop connections, these file sizes can have a big impact on mobile users and on battery life. For example, here are some frameworks and their minified package size:

| Framework | Size |
| ----------- | ------ |
| Angular | 563 kb |
| Ember.js | 340 kb |
| jQuery | 86 kb |
| React | 92.4 kb |
| Vue.js | 91.5 kb |

Those sizes can have a significant impact on your page load and mobile performance.

## The idea

Everyone is concerned about the national shortage of toilet paper caused by the reaction to the coronavirus pandemic. More people being at home during the day along with some panic-stricken hoarders have caused toilet paper to be in much shorter supply than normal.

I'm lucky enough to live in a suburban house with a LOT of people (my wife and 4 kids). This means that we typically keep a decent amount of toilet paper in stock, but even I started to get a little worried, like everyone else, when I saw the pictures of empty store shelves. Six people, with 4 of them under the age of 10 means A LOT of toilet paper usage...but exactly how much is "a lot?"

## The simple solution

So I did what many "dads" (and I'm sure others too, but it's easy for me to make fun of dads as I am one) did: I started counting toilet paper. Every few days I would go around to the various places toilet paper is stored (when you have small children, you always want it within arms reach #dadhack) and count the rolls. Then, I recorded the data in a trusty-old Google Sheet. That was a great start, but I started to think of something fancier - like automated "day zero" calculators, graphs, and other fun statistics.

## Hammer meet nail

As I would advise anyone who wants to learn new technology, it is important to have a problem you want to solve. As a self-taught developer, I've always found it hard to learn a new tool - be it Photoshop or a new Javascript framework - from documentation alone. Having a problem to solve, and then apply the tool to it, has always been the way I learn best. So, I decided to apply Svelte to this problem and learn something new while I scratched my own itch to have a full-fledged TP tracking app.

## Meet the app

Since I already had the Google Sheet in place, I decided to use that as the backend so that could keep updating the count how I had been without investing time in building a whole additional component. The frontend would be easy to host as Svelte compiles down to simple HTML, CSS, and JavaScript.

## Building the app

To build the app, I used Google Sheet's "publish" feature to publish the sheet as raw CSV.  

After that, it was a matter of building the application itself. My plan involved a 4-pane design — the time-to-empty, other statistics like “rolls per month”, a TP graph, and the raw data.  As you can see [in the project source code](https://gitlab.com/brendan-demo/labwork/http-calculator), I made each pane their own component and passed the raw data from the spreadsheet to each.

## Deploy

To deploy the app, I chose to use Netlify so that it would be quickly deployed to a global CDN every time I commit. I already had [GitLab connected](https://www.netlify.com/blog/2016/07/13/gitlab-integration-for-netlify/) to my Netlify account, so it was easy enough to just select the new repository.

From there, I set the build command to

`npm run build`

and the deploy directory to

`public`

Netlify and GitLab take care of the rest, and every time I make a new commit to the master branch, a deploy is enqueued and is live just a few moments later.

If you’d like to track your TP usage to help you determine when you need to place an order, check out the live production app: [tp.labwork.dev](https://tp.labwork.dev/).  You can make your own spreadsheet by making a copy of my [example spreadsheet](https://docs.google.com/spreadsheets/d/1XosX5ZXQMXd9Z6_43_FQwuE2e1EJYibJQJhXN6d3QMA/edit?usp=sharing) that shows the two columns you'll need: date and total rolls remaining. Publish your sheet to the web, then paste that link into the box on tp.labwork.dev and click "go"!

## End Result

In the end, I'm fairly pleased with what I was able to build in just a few hours.  The time-to-empty for my family and I is an entirely different story...

<a href="https://tp.labwork.dev" target="_blank">
    <BlogImage image="/img/blog/http-calc.png" caption="The completed site" />
</a>