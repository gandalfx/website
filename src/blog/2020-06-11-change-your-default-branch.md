---
title:  "Rename your Git default branch from master to main (with GitLab screenshots)"
date:   2020-06-11 05:00:00
# tags: 
#   - gitlab
#   - issues
# image: /img/blog/homereno03.png
author: brendan
type: post
blog: true
excerpt: > 
  We shouldn't use master as a branch name anymore - here's how to change your default branch today.
meta:
  - name: "twitter:title"
    content: "Rename your Git default branch from master to main"
---

> This article was inspired by Scott Hanselman's article about the same topic.

> You can also read my blog post on [changing the default branch stream upstream in git](/blog/2020-06-10-i-was-wrong-about-git-master.html) or [changing your default branch everywhere else](/blog/2020-06-13-change-your-deploy-branch-everywhere.html).

So let's say you're bought in: master is not a great name for the default branch in your repository.  Someone somewhere told you it meant "master" as in "master recording" or "master craftsman," and you just went with it like [I did]() for a long time.  But now you know, it would make way more sense for it to be named something like "main" or "default" or "develop" or "release"...but how do you make that change?

Making the change in your repository is relatively simple - master isn't *really* that different than any other branch, it just happens to be the convention we've used for years.

## 1) Change it locally

To change it, you can use the `move` command in git to copy the entire master branch (and it's history) to a new branch like so:

```
git branch -m master main
```

You can then push it to your remote repository with:

```
git push -u origin main
```

Once you do that, you'll see the option to start a merge request, which we're not going to do because we want `main` to become the default branch, not `master`

<BlogImage
  image="/img/blog/change-default-branch-01.png"
  caption="Output of the commands to move branches"
/>

## 2) Change it on GitLab

Now that the `main` branch exisits on our remote - GitLab - we can change the settings there as well.  On your project, go to `Settings` ➡️ `Repository.`  The top section their is called "Default Branch" - expand it to see the drop down where you can select `main` instead of master.

<BlogImage
  image="/img/blog/change-default-branch-02.png"
  caption="Select the default branch in GitLab"
/>

Then click `Save changes` to save it.

Now, the next time someone clones your repository, they will automatically be on the `main` branch.  

<BlogImage
  image="/img/blog/change-default-branch-03.png"
  caption="Fresh checkout shows the correct default branch"
/>

When they make changes, the link to create a merge request will automatically be pointed at the `main` branch.

<BlogImage
  image="/img/blog/change-default-branch-04.png"
  caption="Merge requests now are against the main branch"
/>

## 3) Other settings

There are other settings that may or may not apply to your repository.  When changing the default branch, you should also check:

* `Settings` ➡️ `Repository` ➡️ `Protected Branches`
* Your `.gitlab-ci.yml` file (or other CI configuration) for any hardcoded references to `master`
* Any other third-party integrations that may rely on the name of the branch `master`

## 4) Remove the master branch

Once you're sure you've gotten any dependencies updated, you can remove the master branch completely.   This will help avoid any confusion around what branch is the "default" branch for developers.  To remove the branch you can go to `https://gitlab.com/username/repository/-/branches` or follow these steps:

* Go to your project and go to `Repository` ➡️ `Branches`
* Under `Active Branches` find `master`
* Click the trash can to the right to delete the branch
* Say "Ok" to the warning about not being able to undo the delete
