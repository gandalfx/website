---
title: Blog
---

Technical blogs can become very dry and `rote`. What if we could make technical writing fun again? Learning by doing is how most folks learn technology. And having a fun problem to solve is the way to make sure you can apply technical concepts to all kinds of issues. I try to remember that in all of my writing.

## Blog Mission Statement
My goal is to help developers understand the importance of CI/CD as a discipline and an art to encourage us all to realize the original promise of "DevOps" and ensure that everyone can contribute.

## Most Recent Posts
<br/>

<BlogPostList 
  :pages="$site.pages" 
  :page-size="$site.themeConfig.pageSize" 
  :start-page="$site.themeConfig.startPage" 
/>
