---
title:  "I was wrong"
date:   2020-06-10 05:00:00
# tags: 
#   - gitlab
#   - issues
# image: /img/blog/homereno03.png
author: brendan
type: post
blog: true
excerpt: > 
  It doesn't matter how we've defined master now, it makes git less inclusive, and it's time to change.  I should have said this before today, but I'm saying it now.
---

I've been using Git for years and always took for granted that the default branch name was `master.` After all, it's been that way since [May 2005](https://github.com/git/git/commit/cad88fdf8d1ebafb5d4d1b92eb243ff86bae740b#diff-8117edf99fe3ee201b23c8c157a64c95R41) long before I started using Git.  At the same time, I've been very supportive of efforts to [eliminate master/slave metaphors in tech](https://tools.ietf.org/id/draft-knodel-terminology-00.html#rfc.section.1.1.1).  I hated using those terms for databases or tools I used in my day-to-day work, but I gave Git a pass.

I was privileged enough to hold this seemingly incongruent dichotomy in my head as "okay."  When pressed and asked, I was happy to accept that Git _didn't_ have slave branches. Thus, the use of the term master must reference a ["master copy"](https://simplicable.com/new/master-copy), like similar terms in art and audio engineering of a master or [mastering](https://en.wikipedia.org/wiki/Mastering_(audio)). I know that I've offered this explanation to folks before, while still being happy to use `master` myself and let other people [choose a different default branch](https://coderwall.com/p/wmizjg/set-the-default-branch-in-a-git-repository) if they wanted to.

Whenever it came up in the Git community at large, this was the accepted answer (as far as I know).  And in general, many tools - GitHub and GitLab, for example - allowed users to change their default branch.  It seemed that it was settled.  But still, the question always itched at my brain: Is it inclusive to use a term like _master_ even if it's not attached to slavery?

Well,even if you think the answer to _master_ question is yes, here's the problem: We're wrong about the intended meaning of `master` in a Git context.  My colleague [James Ramsey](https://twitter.com/_jramsay) who may know more about Git than any human on the planet found [this thread](https://mail.gnome.org/archives/desktop-devel-list/2019-May/msg00066.html), which explains  it is just as likely that `master` is referencing Bitkeeper's master/slave mechanism.  Bitkeeper uses [master/slave termonology](https://github.com/bitkeeper-scm/bitkeeper/blob/master/doc/HOWTO.ask#L223) when referencing repositories.  

Since Git was built as an open-source alternative to Bitkeeper, I think it is safe to say that the origin of `git master` is rooted in master/slave terminology.  Any attempts to now say `master` refers to a master recording, or a master craftsman is a [false etymology](https://en.wikipedia.org/wiki/False_etymology).

It's now clear to me that we have to move off of `master` as the term for the default branch in Git.  There is a lot of effort around this downstream of `git` itself (e.g. setting [defaults in GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/220906), changing [git-for-windows](https://github.com/git-for-windows/git/issues/2674)), but I'm happy to see and support the fact that the upstream Git maintainers are also going to [seriously consider changing master to something else](https://lore.kernel.org/git/20200609223624.GO6569@camp.crustytoothpaste.net/T/#mf6bab91e35ad94fe6ce272615219f887b9b8b440) in Git 3.0.  Also, sooner than a major release, the Git maintainers could choose to add in the ability for `git init` to prompt the user for a default branch.  

We need to make this change, and I was wrong to not push for it sooner.  I need to do better.  I need to make sure that ally is a verb and not a noun in my life.

> If you are neutral in situations of injustice, you have chosen the side of the oppressor.
> ―  Desmond Tutu

Visit [blacklivesmatter.carrd.co](https://blacklivesmatter.carrd.co/) to find out what you can do to make ally a verb for yourself.

#### Update 2020-06-15
Additional evidence that the original reference was to master/slave terminology was found from [a 2005 post](https://marc.info/?l=git&m=111968031816936&w=2) from the creator of Git:

>  What does that mean? It means that in a mirroring schenario, you can, 
>   for each git tree, do:
>
>	(a) On the slave:
>		cat .git/refs/*/* | sort | uniq > slave-ref-list
>
>	(b) On the master:
>		cat .git/refs/*/* | sort | uniq > master-ref-list
>
>	(c) On the master:
>

#### Update 2020-06-018
You can also find out how to change your own default branch name in [GitLab](/blog/2020-06-11-change-your-default-branch.html) and [everywhere else](/blog/2020-06-13-change-your-deploy-branch-everywhere.html) in other blogs I've written.  It's a tiny step, but it is one you can control.  There is so much more we can do - as I mentioned above in the original post, visit [blacklivesmatter.carrd.co](https://blacklivesmatter.carrd.co/) for other more critical actions you can take to help.