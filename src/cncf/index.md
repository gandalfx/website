---
title: CNCF Board Nomination
meta:
  - name: 'twitter:title'
    content: 'My CNCF Board Nomination'
  - name: 'twitter:description'
    content: 'My CNCF Board Nomination as a silver member.'
  - name: 'og:title'
    content: 'My CNCF Board Nomination'
  - name: 'og:description'
    content: 'My CNCF Board Nomination as a silver member.'
---

# CNCF Board Nomination

## Who I am

My name is Brendan O'Leary, and I'm a Senior Developer Evangelist at GitLab.

## My Bio

Brendan O'Leary is a Senior Developer Evangelist at GitLab, the first single application for the DevSecOps lifecycle. That means he connects with developers, contributes to open source projects, and shares his work about cutting-edge technologies on conference panels, at meetups, in contributed articles and on blogs. He has a passion for software development and iterating on processes just as quickly as developers iterate on code. Working with customers to deliver value is what drives Brendan's passion for DevOps and smooth CI/CD implementation. Brendan has worked with a wide range of customers - from the nation's top healthcare institutions to environmental services companies to the Department of Defense. Outside of work, you'll find Brendan with 1 to 4 kids hanging off of him at any given time or occasionally finding a moment alone to build something in his workshop.

## Why I'm running

I joined the CNCF board in 2020 when Priyanka Sharma took the GM role at the CNCF.  Since joining the board, we've seen massive changes in our world and the cloud native ecosystem.  I strongly believe in the power of sustainable open source - something that I've been passionate about my entire career.  Having worked "both sides" of the aisle - vendor companies and end-user companies - including an open core company like GitLab, I have seen the importance and impact of open source software. When projects have healthy communities, passionate and well-supported maintainers, and a broad audience they are set up for success. I’m especially interested in driving the effort to define and set the bar for end-user driven open source. 

In addition, I hope to bring to bear my unique experience with remote work leadership to the CNCF as we enter an uncertain 2021.  While in-person events have defined the CNCF’s past and serve a critical role in bringing communities together, the diversity and inclusiveness that virtual and hybrid events allow should be something we look to build upon in the future.  GitLab has been leading the way with remote work even before the pandemic, and my unique perspective can help the CNCF shape the future of hybrid events.  This will allow the CNCF to create a sustainable model for itself and expand its reach across borders and to people who would have otherwise not have access to CNCF events. I hope to continue serving on the board to help the CNCF build sustainable cloud native software ecosystems.
